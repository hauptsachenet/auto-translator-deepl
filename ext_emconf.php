<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Translator DeepL',
    'description' => 'Translates your content automatically via DeepL API',
    'category' => 'be',
    'author' => 'Kay Wienöbst',
    'author_email' => 'kay@hauptsache.net',
    'state' => 'stable',
    'uploadfolder' => 0,
    'clearCacheOnLoad' => 1,
    'version' => '3.0.0',
    'constraints' => [
        'depends' => [
            'auto_translator' => '*'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
