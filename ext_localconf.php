<?php

defined('TYPO3_MODE') or die();

$boot = function () {
    if (TYPO3_MODE === 'BE' && !(TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_INSTALL)) {
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class)
            ->connect(
                \TYPO3\CMS\Backend\Backend\ToolbarItems\SystemInformationToolbarItem::class,
                'loadMessages',
                \Hn\AutoTranslatorDeepL\Controller\DeepLApiUsageInformation::class,
                'appendMessage'
            );
    }

    \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
        ->registerImplementation(
            \Hn\AutoTranslator\Service\TranslatorInterface::class,
            \Hn\AutoTranslatorDeepL\Service\DeepLTranslator::class
        );
};

$boot();
unset($boot);
