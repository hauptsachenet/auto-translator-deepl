<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 15.06.18
 * Time: 10:54
 */

namespace Hn\AutoTranslatorDeepL\Utility;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ExtensionManagerConfiguration
{
    /**
     * @param $propertyPath
     * @return mixed
     */
    public static function getProperty($propertyPath)
    {
        return GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('auto_translator_deepl', $propertyPath);
    }
}