<?php

/**
 * Created by PhpStorm.
 * User: kay
 * Date: 25.06.18
 * Time: 16:50
 */

namespace Hn\AutoTranslatorDeepL\Controller;

use Hn\AutoTranslatorDeepL\Service\DeepLApiService;
use TYPO3\CMS\Backend\Backend\ToolbarItems\SystemInformationToolbarItem;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DeepLApiUsageInformation extends ActionController
{
    /**
     * @param $number
     * @return string
     */
    private static function formatNumber($number)
    {
        return number_format($number, 0, '', ' ');
    }

    /**
     * Adds deepL usage to the system information toolbar item
     *
     * @param SystemInformationToolbarItem $systemInformationToolbarItem
     */
    public function appendMessage(SystemInformationToolbarItem $systemInformationToolbarItem)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var DeepLApiService $translationService */
        $deepLApiService = $objectManager->get(DeepLApiService::class);

        $usage = $deepLApiService->usage();
        if (!$usage) {
            return;
        }

        [$count, $limit] = array_values($usage);
        $systemInformationToolbarItem->addSystemInformation('DeepL usage', sprintf('%s / %s', self::formatNumber($count), self::formatNumber($limit)), 'actions-localize');
    }
}