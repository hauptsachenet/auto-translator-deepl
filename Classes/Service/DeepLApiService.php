<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 15.06.18
 * Time: 12:28
 */

namespace Hn\AutoTranslatorDeepL\Service;

use GuzzleHttp\Exception\ClientException;
use Hn\AutoTranslatorDeepL\Utility\ExtensionManagerConfiguration;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

class DeepLApiService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var RequestFactory
     */
    protected $requestFactory;

    /**
     * DeepLApiService constructor.
     */
    public function __construct()
    {
        $this->requestFactory = GeneralUtility::makeInstance(RequestFactory::class);
    }

    /**
     * translates a text string
     *
     * @param string $text
     * @param string $targetLanguage
     * @param string|null $sourceLanguage
     * @return string
     */
    public function translate(string $text, string $targetLanguage, ?string $sourceLanguage = null): string
    {
        $url = ExtensionManagerConfiguration::getProperty('api/url') . 'translate';

        $parameters = [];
        $parameters['auth_key'] = ExtensionManagerConfiguration::getProperty('api/auth_key');
        $parameters['text'] = $text;
        $parameters['target_lang'] = $targetLanguage;
        $parameters['tag_handling'] = 'xml';
        if ($sourceLanguage) {
            $parameters['source_lang'] = $sourceLanguage;
        }

        try {
            $response = $this->requestFactory->request($url, 'GET', [
                'form_params' => $parameters
            ]);

            if ($response->getStatusCode() !== 200) {
                throw new DeepLApiServiceException('Expected response status code 200, but got ' . $response->getStatusCode());
            }

            if (strpos($response->getHeaderLine('Content-Type'), 'application/json') !== 0) {
                throw new DeepLApiServiceException('Expected Content-Type application/json, but got ' . $response->getHeaderLine('Content-Type'));
            }

            $content = json_decode($response->getBody()->getContents(), true);
            $translation = ObjectAccess::getPropertyPath($content, 'translations.0.text');
            if ($translation) {
                return $translation;
            }
        } catch (ClientException $e) {
            $this->logger->critical($e->getMessage());
        } catch (DeepLApiServiceException $e) {
            $this->logger->critical($e->getMessage());
        }

        // if no translation found return input text
        return $text;
    }

    /**
     * returns usage information
     *
     * @return array
     */
    public function usage()
    {
        $result = [];

        $url = ExtensionManagerConfiguration::getProperty('api/url') . 'usage';

        $parameters = [];
        $parameters['auth_key'] = ExtensionManagerConfiguration::getProperty('api/auth_key');

        try {
            $response = $this->requestFactory->request($url, 'GET', [
                'form_params' => $parameters
            ]);

            if ($response->getStatusCode() !== 200) {
                throw new DeepLApiServiceException('Expected response status code 200, but got ' . $response->getStatusCode());
            }

            $content = json_decode($response->getBody()->getContents(), true);

            $characterCount = ObjectAccess::getPropertyPath($content, 'character_count');
            $characterLimit = ObjectAccess::getPropertyPath($content, 'character_limit');


            if ($characterCount) {
                $result['characterCount'] = $characterCount;
            }
            if ($characterLimit) {
                $result['characterLimit'] = $characterLimit;
            }

        } catch (ClientException $e) {
            $this->logger->critical($e->getMessage());
        } catch (DeepLApiServiceException $e) {
            $this->logger->critical($e->getMessage());
        }

        return $result;
    }
}