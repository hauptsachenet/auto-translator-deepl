<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 13.06.18
 * Time: 16:35
 */

namespace Hn\AutoTranslatorDeepL\Service;

use Hn\AutoTranslator\Service\TranslatorInterface;

class DeepLTranslator implements TranslatorInterface
{
    /**
     * @var \Hn\AutoTranslatorDeepL\Service\DeepLApiService
     */
    protected $deepLApiService;

    /**
     * DeepLTranslator constructor.
     * @param DeepLApiService $deepLApiService
     */
    public function __construct(DeepLApiService $deepLApiService)
    {
        $this->deepLApiService = $deepLApiService;
    }

    /**
     * @param string $content the content to be translated
     * @param string $targetLanguageIsoCode the target ISO 639-1 code
     * @param string|null $sourceLanguageIsoCode the source ISO 639-1 code
     * @return string
     */
    public function translate(string $text, string $targetLanguageIsoCode, string $sourceLanguageIsoCode): string
    {
        return $this->deepLApiService->translate($text, $targetLanguageIsoCode, $sourceLanguageIsoCode);
    }
}